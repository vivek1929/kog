package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/blang/semver"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
)

var (
	listVersions  bool
	switchVersion string
	tmpBin        string
	minVersion    = semver.MustParse("1.4.0")
	versions      = make(semver.Versions, 0)
	gitlabClient  *gitlab.Client
	releaseMap    = make(map[string]*gitlab.Release)
)

func setupUpgradeCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:     "switch-to",
		Short:   "switch-to",
		Aliases: []string{"upgrade"},
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		level.Debug(baseLogger).Log("msg", "upgrade PersistentPreRun called")
		gitlabClient = gitlab.NewClient(nil, "")
		if err := gitlabClient.SetBaseURL("https://gitlab.com/api/v4"); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		versions = make(semver.Versions, 0)
		releases, _, err := gitlabClient.Releases.ListReleases(gitlabProject, &gitlab.ListReleasesOptions{})
		if err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		for _, release := range releases {
			version, _ := semver.ParseTolerant(release.TagName)
			if version.LT(minVersion) {
				continue
			}
			versions = append(versions, version)
			releaseMap[version.String()] = release
			if versions.Len() == 10 {
				break
			}
		}
		semver.Sort(versions)
		for left, right := 0, versions.Len()-1; left < right; left, right = left+1, right-1 {
			versions[left], versions[right] = versions[right], versions[left]
		}

		if switchVersion == "latest" {
			if versions.Len() == 0 {
				level.Error(baseLogger).Log("err", fmt.Sprintf("No versions found"), "min-version", minVersion.String())
				os.Exit(127)
			}
			switchVersion = versions[0].String()
		}
		parsedVersion, _ := semver.ParseTolerant(switchVersion)
		switchVersion = parsedVersion.String()
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		if listVersions {
			printVersions()
			return
		}
		upgradeVersion()
	}

	cmd.PersistentFlags().StringVarP(
		&switchVersion,
		"version",
		"v",
		"latest",
		"version to switch to")

	cmd.PersistentFlags().BoolVarP(
		&listVersions,
		"list",
		"",
		listVersions,
		"list all hosted versions")

	return
}

func printVersions() {
	for _, rversion := range versions {
		if rversion.String() == strings.TrimLeft(version, "v") {
			fmt.Println(fmt.Sprintf("> %s", rversion.String()))
		} else {

			fmt.Println(rversion.String())
		}
	}
}

func upgradeVersion() {
	level.Info(baseLogger).Log("msg", "Switching to Version", "new-version", switchVersion)

	if semver.MustParse(switchVersion).LT(minVersion) {
		level.Error(baseLogger).Log("err", "Version lower then minimum", "new-version", switchVersion, "min-version", minVersion.String())
		os.Exit(127)
	}

	tmpBin = filepath.Join(filepath.Dir(binPath), switchVersion)
	if release, ok := releaseMap[switchVersion]; !ok {
		level.Error(baseLogger).Log("msg", "Version not found", "new-version", switchVersion)
		os.Exit(127)
	} else {
		for _, link := range release.Assets.Links {
			if link.Name == binName {
				level.Debug(baseLogger).Log("link", link.URL, "bin", binPath)
				if err := downloadFile(link.URL); err != nil {
					level.Error(baseLogger).Log("err", err)
					os.Exit(127)
				}
				if err := os.Rename(tmpBin, binPath); err != nil {
					level.Error(baseLogger).Log("err", err)
					os.Exit(127)
				}
				break
			}
		}
	}
}

func downloadFile(url string) (err error) {
	var (
		out  *os.File
		resp *http.Response
	)
	if out, err = os.Create(tmpBin); err != nil && !os.IsExist(err) {
		return
	} else if os.IsExist(err) {
		if out, err = os.Open(tmpBin); err != nil {
			return
		}
	}
	defer out.Close()
	if resp, err = http.Get(url); err != nil {
		return
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}
	if _, err = io.Copy(out, resp.Body); err != nil {
		level.Error(baseLogger).Log("err", err)
	}

	err = out.Chmod(0770)

	return
}
