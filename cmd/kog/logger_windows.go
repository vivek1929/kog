// +build windows

package main

import (
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
)

func setupLogger(cfg *viper.Viper) (logger log.Logger) {
	cfg.SetDefault("logger.formatter", "logfmt")
	cfg.SetDefault("logger.level", "info")
	switch cfg.GetString("logger.formatter") {
	case "json":
		logger = log.NewJSONLogger(os.Stderr)
	case "logfmt":
		logger = log.NewLogfmtLogger(os.Stderr)
	default:
		logger = log.NewLogfmtLogger(os.Stderr)
		logger.Log("err", "Unknown Log Formatter", "logger.formatter", cfg.GetString("logger.formatter"))
		os.Exit(127)
	}

	lvl := cfg.GetString("logger.level")
	if cfg.GetBool("debug") {
		lvl = "debug"
	}

	switch lvl {
	case "none":
		logger = level.NewFilter(logger, level.AllowNone())
	case "error":
		logger = level.NewFilter(logger, level.AllowError())
	case "warn":
		logger = level.NewFilter(logger, level.AllowWarn())
	case "debug":
		logger = level.NewFilter(logger, level.AllowDebug())
	default:
		logger = level.NewFilter(logger, level.AllowInfo())
	}

	logger = level.NewInjector(logger, level.DebugValue())

	logger = log.With(
		logger,
		"timestamp", log.DefaultTimestampUTC,
	)

	return
}
