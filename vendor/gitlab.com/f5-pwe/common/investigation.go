/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pwe

import (
	"encoding/json"
	"time"
)

type Investigation struct {
	StartDate   *time.Time          `json:"start_date,omitempty" yaml:"start_date,omitempty"`
	UpdatedDate *time.Time          `json:"updated_date,omitempty" yaml:"updated_date,omitempty"`
	EndDate     *time.Time          `json:"end_date,omitempty" yaml:"end_date,omitempty"`
	StartedBy   *User               `json:"started_by,omitempty" yaml:"started_by,omitempty"`
	Notes       Notes               `json:"notes,omitempty" yaml:"notes,omitempty"`
	Result      InvestigationStatus `json:"result,omitempty" yaml:"result,omitempty"`
}

type Note struct {
	Date      *time.Time `json:"date,omitempty" yaml:"date,omitempty"`
	EnteredBy *User      `json:"entered_by,omitempty" yaml:"entered_by,omitempty"`
	Text      string     `json:"text,omitempty" yaml:"text,omitempty"`
}

type Notes = []Note

type InvestigationStatus int

var investigationStatusStrings = [...]string{
	"active",
	"product bug",
	"framework bug",
	"platform bug",
	"unknown",
	"success",
}

const (
	INVESTIGATION_ACTIVE InvestigationStatus = iota
	INVESTIGATION_PRODUCT
	INVESTIGATION_FRAMEWORK
	INVESTIGATION_PLATFORM
	INVESTIGATION_UNKNOWN
	INVESTIGATION_SUCCESS
)

func (is InvestigationStatus) String() string {
	return jobStatusStrings[is]
}

func InvestigationStatusFromString(val string) InvestigationStatus {
	for idx, statusString := range investigationStatusStrings {
		if statusString == val {
			return InvestigationStatus(idx)
		}
	}

	return INVESTIGATION_UNKNOWN
}

func (is InvestigationStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(is.String())
}

func (is *InvestigationStatus) UnmarshalJSON(b []byte) (err error) {
	var val string
	err = json.Unmarshal(b, &val)
	if err != nil {
		*is = INVESTIGATION_UNKNOWN
		return
	}

	*is = InvestigationStatusFromString(val)
	return
}
