package main

import (
	"fmt"

	"github.com/go-kit/kit/log/level"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func setupVersionCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "version",
		Short: "display version info",
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		level.Debug(baseLogger).Log("msg", "version PersistentPreRun called")
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		fmt.Println(fmt.Sprintf("%s version %s", pkgName, version))
	}

	return
}
