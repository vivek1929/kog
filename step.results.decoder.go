package kog

import (
	"encoding/json"
	"fmt"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"

	"gitlab.com/f5-pwe/common/actions"
	"gitlab.com/f5-pwe/common/variables"
)

func DecodeResults(result []byte, logger log.Logger) (actions.ActionResult, error) {
	resultStr := string(result)
	if resultStr == "" {
		err := fmt.Errorf("result is empty")
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}
	if resultStr == "{}" {
		err := fmt.Errorf("JSON result is empty")
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}
	var stepRunResult actions.ActionResult
	if err := json.Unmarshal(result, &stepRunResult); err != nil {
		err = errors.Wrap(err, "parsing JSON")
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}

	// temporarily get context from root node
	// this will return an error once all containers are updated
	if stepRunResult.Context == nil {
		var ctx variables.Context
		if err := json.Unmarshal(result, &ctx); err != nil {
			return NewStepResult(actions.ACTION_ABORT, err.Error()), err
		}
		level.Error(logger).Log("msg", "DEPRECATED: context loaded from root node")
		delete(ctx, "code")
		delete(ctx, "message")
		delete(ctx, "result")
		stepRunResult.Context = ctx
	}

	return stepRunResult, nil
}

func DecodeStepResults(ctx variables.Context, logger log.Logger) (results []actions.ActionResult, err error) {
	var (
		b []byte
	)
	r, ok := ctx["step_run_results"]
	if !ok {
		level.Debug(logger).Log("msg", "Initializing step run results")
		return []actions.ActionResult{}, nil
	}

	// not the best solution, but works reliably
	// we need to specifically unmarshal step_run_results
	b, err = json.Marshal(r)
	if err != nil {
		return nil, fmt.Errorf("serializing step run results: %v", err)
	}
	if err = json.Unmarshal(b, &results); err != nil {
		return nil, fmt.Errorf("parsing step run results: %v", err)
	}

	return results, nil
}
