package main

import (
	"strings"

	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"

	"gitlab.com/f5-pwe/kog"
)

func setupNotifyHooks(cfg *viper.Viper) (notifiers *kog.NotifyHooks) {
	notifiers = &kog.NotifyHooks{}

	for _, hook := range cfg.GetStringSlice("notifiers") {
		if strings.HasPrefix(hook, "http") {
			notifiers.AddNotification(kog.NewHTTPNotifyHook(hook, baseLogger))
			level.Debug(baseLogger).Log("msg", "Added http notify hook", "notifier", hook)
		} else if hook == "console" {
			notifiers.AddNotification(kog.NewConsoleHook(baseLogger))
			level.Debug(baseLogger).Log("msg", "Added console notify hook")
		} else {
			level.Error(baseLogger).Log("err", "Unknown Notifier Type", "notifier", hook)
		}
	}

	return
}
