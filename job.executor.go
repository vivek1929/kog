package kog

import (
	"fmt"
	"runtime/debug"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/actions"
	"gitlab.com/f5-pwe/common/workflow"
)

func ExecuteJob(executor Executor, job *pwe.Job, logger log.Logger, notifierHooks *NotifyHooks, extraEnv map[string]string, volumes []string) (retval JobResult) {
	retval = JobResult{Name: job.Name, ID: job.ID.String()}
	var (
		err  error
		step workflow.WorkflowStep
	)

	defer func() {
		if r := recover(); r != nil {
			level.Error(logger).Log("msg", "Panic in ExecuteJob", "err", r, "traceback", debug.Stack())
		}
	}()

	if err = executor.Prepare(job); err != nil {
		level.Error(logger).Log("msg", "Error preparing environment to execute job", "err", err)
		retval.Status = pwe.JOB_FAILURE
		retval.Message = "Error preparing environment to execute job"
		return
	}

	level.Debug(logger).Log("msg", "Starting Workflow", "job", fmt.Sprintf("%+v", job))
	job.Status = pwe.JOB_RUNNING

	next := "start"
	for {
		if step, err = job.Workflow().Step(next); err != nil {
			retval.Status = pwe.JOB_FAILURE
			retval.Message = fmt.Sprintf("unknown next step: \"%s\"", next)
			break
		}
		stepLogger := log.With(logger, "step", step.Name)
		job.CurrentStep = &step

		startTime := time.Now()
		result := ExecuteStep(executor, step, job, *job.Context, stepLogger, notifierHooks, extraEnv, volumes)
		duration := time.Since(startTime)
		level.Debug(logger).Log(
			"msg", "Step Finished",
			"step", step.Name,
			"result", result.Result.String(),
			"return_code", result.ReturnCode,
			"result_message", result.Message,
			"notify_status", result.NotifyStatus,
			"duration", duration,
		)

		if len(result.Context) != 0 {
			// Pass the context to the next step
			job.Context = &result.Context
		} else {
			level.Warn(stepLogger).Log("msg", "Step result context is empty")
		}

		if ctx := *job.Context; ctx != nil {
			// append to the step result list
			rs := actions.ActionResult{
				Result:     result.Result,
				ReturnCode: result.ReturnCode,
				Message:    result.Message,
				Name:       result.Name,
				Duration:   duration.Seconds(),
			}
			results, err := DecodeStepResults(ctx, stepLogger)
			if err != nil {
				level.Error(stepLogger).Log("msg", "Could not decode step run result", "err", err)
			} else {
				ctx["step_run_results"] = append(results, rs)
				level.Debug(stepLogger).Log("msg", "Step run results", "results", fmt.Sprintf("%+v", ctx["step_run_results"]))
			}
		}

		if result.NotifyStatus != "" {
			retval.NotifyStatus = result.NotifyStatus
			retval.NotifyMessage = result.Message
		} else if result.Result == actions.ACTION_ABORT {
			// aborted steps are not used for workflow control and should result in a failed job
			retval.NotifyStatus = pwe.JOB_FAILURE.String()
			retval.NotifyMessage = result.Message
		}

		next = NextStep(step, result.Result)
		retval.Message = result.Message
		if next == "" {
			switch result.Result {
			case actions.ACTION_SUCCESS:
				retval.Status = pwe.JOB_SUCCESS
			case actions.ACTION_FAIL:
				retval.Status = pwe.JOB_FAILURE
			case actions.ACTION_ABORT:
				retval.Status = pwe.JOB_FAILURE
			}
			break
		}

		stepLogger = log.With(stepLogger, "message", result.Message)
		level.Debug(stepLogger).Log("result", fmt.Sprintf("%+v", result))
		switch result.Result {
		case actions.ACTION_FAIL:
			level.Warn(stepLogger).Log("msg", "Step Failed")
		case actions.ACTION_ABORT:
			level.Warn(stepLogger).Log("msg", "Step Aborted")
		case actions.ACTION_SUCCESS:
			level.Info(stepLogger).Log("msg", "Step Success")
		}

		// stop the job execution after the first failure is encountered
		// useful for investigating failing workflows
		// we'd want to reset it every time though, since we want to job to run to completion eventually
		if result.Result != actions.ACTION_SUCCESS && job.StopOnFailure {
			level.Warn(stepLogger).Log("msg", "Stopping workflow since 'stop on failure' flag was set")
			retval.Status = pwe.JOB_UNDER_INVESTIGATION
			job.StopOnFailure = false
			break
		}
	}

	// no more workflow steps left
	job.CurrentStep = &workflow.WorkflowStep{}
	return retval
}
