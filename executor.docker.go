package kog

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/digitalxero/slugify"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"
	pwe "gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/actions"
)

type dockerExecutor struct {
	wc         *wrappedClient
	rootLogger log.Logger

	sendSTDIn bool
	logger    log.Logger
	job       *dockerJob
	done      *sync.WaitGroup
}

func NewDockerExecutor(logger log.Logger) (e Executor, err error) {
	var wc *wrappedClient
	if wc, err = newWrappedDockerClient(); err != nil {
		level.Error(logger).Log("msg", "Error initializing Docker connection", "err", err)
		return
	}
	e = &dockerExecutor{
		rootLogger: logger,
		logger:     logger,
		wc:         wc,
	}

	return
}

func (de *dockerExecutor) GetJob() Job {
	return de.job
}

func (de *dockerExecutor) CreateJob(inputData, correlationId, jobName, stepName, runId string, action actions.Action, labels map[string]string, volumes []string) (err error) {
	var (
		containerId string
	)

	containerName := slugify.Slugify(fmt.Sprintf("%s-%s-%s-%s", correlationId, jobName, stepName, runId), 0)
	action.Env["KOG_CONTEXT"] = base64.StdEncoding.EncodeToString([]byte(inputData))
	containerId, de.sendSTDIn, err = de.wc.CreateContainer(
		containerName,
		action.Image,
		action.Tag,
		action.Command,
		action.Args,
		action.Env,
		volumes,
		labels,
		de.rootLogger,
	)
	if err != nil {
		level.Error(de.logger).Log("err", err)
		return
	}

	de.job = &dockerJob{name: jobName, containerId: containerId}
	de.logger = log.With(de.rootLogger, "container_id", containerId, "container", jobName)
	de.done = &sync.WaitGroup{}
	err = de.wc.StartContainer(de.job.ID())

	return
}

func (de *dockerExecutor) AttachIO() (err error) {
	var (
		input, output types.HijackedResponse
	)

	if de.sendSTDIn {
		if input, err = de.wc.AttachContainerStdin(de.job.ID()); err != nil {
			return
		}
		de.job.Input(input.Conn)
	}

	if output, err = de.wc.AttachContainerOutput(de.job.ID()); err != nil {
		return
	}
	de.job.Output(ioutil.NopCloser(output.Reader))

	return
}

func (de *dockerExecutor) SendInput(inputData string) (err error) {
	if !de.sendSTDIn {
		return
	}

	var writer = de.job.input
	defer writer.Close()

	bufin := bufio.NewWriter(writer)
	if _, err = bufin.WriteString(inputData); err != nil {
		return
	}

	return bufin.Flush()
}

func (de *dockerExecutor) ProcessOutput(resultText *string) {
	outR, outW := io.Pipe()
	outTee := ioutil.NopCloser(io.TeeReader(outR, os.Stdout))
	reader := bufio.NewReader(outTee)

	// container output demuxing goroutine
	de.done.Add(1)
	go func() {
		defer outW.Close()
		defer de.done.Done()
		if _, err := stdcopy.StdCopy(outW, os.Stderr, de.job.output); err != nil {
			level.Error(de.logger).Log("msg", "Error in container output streaming", "err", err)
		}

		level.Debug(de.logger).Log("msg", "Step standard streams copied")
	}()

	de.done.Add(1)
	// container output scanning goroutine
	go func() {
		defer outTee.Close()
		defer de.done.Done()
		for {
			line, err := reader.ReadString('\n')

			if strings.HasPrefix(line, "KOG:") {
				rt := strings.TrimLeft(line, "KOG:")
				rt = strings.TrimSpace(rt)
				level.Debug(de.logger).Log("msg", "Got result", "raw_result", rt)
				*resultText = rt
				_, err := ioutil.ReadAll(outTee); if err != nil {
                                level.Debug(de.logger).Log("err", err)}
				break
			} else {
				level.Debug(de.logger).Log("msg", line)
			}

			if err == io.EOF {
				level.Error(de.logger).Log("msg", "Leaving output scanning, got EOF from the docker logs before a result was generated!")
				break
			} else if err != nil {
				level.Error(de.logger).Log("msg", "Error processing container output", "err", err)
			}
		}
	}()
}

func (de *dockerExecutor) Wait(timeout time.Duration) (exitCode int, err error) {
	if timedout := waitTimeout(de.done, timeout); timedout {
		level.Error(de.logger).Log("msg", fmt.Sprintf("step \"%v\" timed out after %v", de.job.name, timeout))

		if err = de.wc.KillContainer(de.job.ID()); err != nil && !strings.Contains(err.Error(), "No such container") {
			level.Error(de.logger).Log("msg", "Error killing container", "err", err)
		} else if err != nil {
			level.Error(de.logger).Log("err", err)
			return
		}
		err = fmt.Errorf("step timeout")
		return
	}

	level.Info(de.logger).Log("msg", "Waiting for container to exit...")
	if exitCode, err = de.wc.WaitForContainerExitWithTimeout(de.job.ID(), containerExitTimeout); err != nil {
		if strings.Contains(err.Error(), context.DeadlineExceeded.Error()) {
			err = errors.Wrap(err, fmt.Sprintf("Container exit timed out after %v", containerExitTimeout))
		}
		level.Error(de.logger).Log("msg", "Error in container exit", "err", err, "exitCode", exitCode)
		return
	}

	return 0, nil
}

func (de *dockerExecutor) Cleanup() (err error) {
	if de.job != nil {
		if err = de.wc.RemoveContainer(de.job.ID()); err != nil && !strings.Contains(err.Error(), "No such container") {
			level.Error(de.logger).Log("msg", "Failed to remove container after successful execution", "err", err)
		} else {
			level.Debug(de.logger).Log("msg", "Container Removed")
		}
	}

	de.job = nil
	de.logger = nil
	de.done = nil
	return
}

func (de *dockerExecutor) Prepare(job *pwe.Job) (err error) {
	level.Info(de.rootLogger).Log("msg", "Pulling docker images")
	ch := make(chan struct{})
	for _, v := range job.Actions {
		go func(v actions.Action) {
			level.Debug(de.rootLogger).Log("msg", "Pulling image", "image", v.Image, "tag", v.Tag, "action", v.Name)
			tag := "latest"
			if v.Tag != "" {
				tag = v.Tag
			}
			if perr := de.wc.PullImage(v.Image, tag, de.rootLogger); err != nil {
				level.Error(de.rootLogger).Log("msg", "Error pulling image", "err", perr)
				err = perr
			}
			ch <- struct{}{}
		}(v)
	}
	for range job.Actions {
		<-ch
	}
	return
}
