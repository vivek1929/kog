/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kog

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/drone/envsubst"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

type wrappedClient struct {
	cli *client.Client
}

func newWrappedDockerClient() (wc *wrappedClient, err error) {
	wc = &wrappedClient{}
	wc.cli, err = client.NewClientWithOpts(client.FromEnv)

	return
}

func (wc *wrappedClient) PullImage(imageName string, tag string, logger log.Logger) error {
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	pullLogger := level.Error(log.With(logger, "image", imageName, "tag", imageTag))
	opts := types.ImagePullOptions{}
	image := fmt.Sprintf("%s:%s", imageName, imageTag)
	output, err := wc.cli.ImagePull(context.Background(), image, opts)
	if err != nil {
		return err
	}
	outdec := json.NewDecoder(output)
	type statusLine struct {
		Id       string `json:"id"`
		Status   string `json:"status"`
		Progress string `json:"progress"`
		Error    string `json:"error"`
	}
	for {
		line := statusLine{}
		if err := outdec.Decode(&line); err == io.EOF {
			break
		} else if err != nil {
			pullLogger.Log("err", err)
		} else {
			if line.Error != "" {
				return fmt.Errorf("%v", line.Error)
			}
		}
	}
	return nil
}

func (wc *wrappedClient) CreateContainer(containerName string, imageName string, tag string, command string, args []string, env map[string]string, volumes []string, labels map[string]string, logger log.Logger) (containerID string, useSTDIn bool, err error) {
	var resp container.ContainerCreateCreatedBody
	useSTDIn = true
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	createLogger := level.Error(log.With(logger, "image", imageName, "tag", imageTag))

	image := fmt.Sprintf("%s:%s", imageName, imageTag)
	imageInfo, body, err := wc.cli.ImageInspectWithRaw(context.Background(), image)
	if err != nil && client.IsErrNotFound(err) {
		if err = wc.PullImage(imageName, tag, logger); err != nil {
			createLogger.Log("msg", "Failed to pull missing image", "err", err)
			return
		}
		imageInfo, _, err = wc.cli.ImageInspectWithRaw(context.Background(), image)
	}

	if err != nil {
		if body != nil {
			createLogger.Log("msg", "image details", "body", string(body))
		}

		createLogger.Log("msg", "Error fetching image details", "err", err)
		return
	}

	containerEnv := []string{}
	for k, v := range env {
		containerEnv = append(containerEnv, fmt.Sprintf("%s=%s", k, v))
		os.Setenv(k, v)
	}

	if imageInfo.Config != nil {
		for _, imgEnv := range imageInfo.Config.Env {
			kv := strings.Split(imgEnv, "=")
			os.Setenv(kv[0], kv[1])
			if kv[0] == "KOG_ENV_CONTEXT" && kv[1] == "true" {
				useSTDIn = false
			}
		}
	}
	for idx, eEnv := range containerEnv {
		if eEnv, err := envsubst.EvalEnv(eEnv); err == nil {
			containerEnv[idx] = eEnv
		}
	}
	containerLabels := make(map[string]string)
	for k, v := range labels {
		containerLabels[k] = v
	}

	stopTimeout, _ := strconv.Atoi(containerExitTimeout.String())
	if stopTimeout < 10 {
		stopTimeout = 10
	}

	containerConf := container.Config{
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Tty:          false,
		OpenStdin:    true,
		StdinOnce:    true,
		Env:          containerEnv,
		Image:        image,
		StopSignal:   "SIGTERM",
		StopTimeout:  &stopTimeout,
		Labels:       containerLabels,
	}
	level.Debug(createLogger).Log("msg", "Container Config", "config", fmt.Sprintf("%+v", containerConf))
	if command != "" {
		cmd := []string{command}
		cmd = append(cmd, args...)
		containerConf.Cmd = cmd
	} else {
		if len(args) > 0 {
			cmd := imageInfo.Config.Cmd
			cmd = append(cmd, args...)
			containerConf.Cmd = cmd
		}
	}
	level.Debug(createLogger).Log("msg", "Container command", "cmd", fmt.Sprintf("%v", containerConf.Cmd))

	hostConf := container.HostConfig{}
	if volumes != nil {
		for idx, vol := range volumes {
			if vol, err := envsubst.EvalEnv(vol); err == nil {
				volumes[idx] = vol
			}
		}
		hostConf.Binds = volumes
	}

	netConf := network.NetworkingConfig{}
	if resp, err = wc.cli.ContainerCreate(context.Background(), &containerConf, &hostConf, &netConf, containerName); err != nil {
		return
	}
	containerID = resp.ID

	return
}

func (wc *wrappedClient) AttachContainerStdin(containerId string) (types.HijackedResponse, error) {
	attachOpts := types.ContainerAttachOptions{
		Stream: true,
		Stdin:  true,
		Stdout: false,
		Stderr: false,
	}
	resp, err := wc.cli.ContainerAttach(
		context.Background(),
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func (wc *wrappedClient) AttachContainerOutput(containerId string) (types.HijackedResponse, error) {
	attachOpts := types.ContainerAttachOptions{
		Stream: true,
		Stdin:  false,
		Stdout: true,
		Stderr: true,
	}
	resp, err := wc.cli.ContainerAttach(
		context.Background(),
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func (wc *wrappedClient) StartContainer(containerId string) error {
	if err := wc.cli.ContainerStart(context.Background(), containerId, types.ContainerStartOptions{}); err != nil {
		return err
	}
	return nil
}

func (wc *wrappedClient) WaitForContainerExitWithTimeout(containerId string, timeout time.Duration) (statusCode int, err error) {
	resp, err := wc.cli.ContainerInspect(context.Background(), containerId)
	if err != nil {
		return -1, err
	}

	state := resp.State
	if state != nil && state.Status == "exited" {
		return state.ExitCode, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	result, werr := wc.cli.ContainerWait(ctx, containerId, container.WaitConditionNotRunning)
	statusCode = 99
	select {
	case <-ctx.Done():
		err = ctx.Err()
	case out := <-result:
		statusCode = int(out.StatusCode)
		if out.Error != nil {
			err = fmt.Errorf(out.Error.Message)
		}
	case err = <-werr:
		break
	}

	return
}

func (wc *wrappedClient) GetContainerStdout(containerId string) (io.ReadCloser, error) {
	stdoutOpts := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: false,
		Timestamps: false,
	}
	return wc.cli.ContainerLogs(context.Background(), containerId, stdoutOpts)
}

func (wc *wrappedClient) GetContainerStderr(containerId string) (io.ReadCloser, error) {
	stderrOpts := types.ContainerLogsOptions{
		ShowStdout: false,
		ShowStderr: true,
	}
	return wc.cli.ContainerLogs(context.Background(), containerId, stderrOpts)
}

func (wc *wrappedClient) RemoveContainer(containerId string) error {
	opts := types.ContainerRemoveOptions{
		Force:         false,
		RemoveVolumes: false,
		RemoveLinks:   false,
	}
	if err := wc.cli.ContainerRemove(context.Background(), containerId, opts); err != nil && client.IsErrNotFound(err) {
		return err
	}
	return nil
}

func (wc *wrappedClient) KillContainer(containerId string) error {
	if err := wc.cli.ContainerKill(context.Background(), containerId, "SIGKILL"); err != nil && client.IsErrNotFound(err) {
		return err
	}
	return nil
}
