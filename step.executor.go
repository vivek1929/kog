package kog

import (
	"encoding/json"
	"fmt"
	_ "io"
	"runtime/debug"
	"time"

	"github.com/digitalxero/slugify"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/actions"
	"gitlab.com/f5-pwe/common/variables"
	"gitlab.com/f5-pwe/common/workflow"
)

const (
	defaultTimeout = time.Hour * 24
)

func ExecuteStep(executor Executor, step workflow.WorkflowStep, job *pwe.Job, ctx variables.Context,
	logger log.Logger, notifierHooks *NotifyHooks, extraEnv map[string]string, volumes []string) actions.ActionResult {
	var (
		err           error
		action        actions.Action
		result        actions.ActionResult
		inputJsonData []byte
		//input io.WriteCloser
		//output io.ReadCloser
		exitCode int
	)

	defer func() {
		_ = executor.Cleanup()
		if r := recover(); r != nil {
			level.Error(logger).Log("msg", "Panic in ExecuteStep", "err", r, "traceback", debug.Stack())
		}
	}()

	actionLogger := log.With(logger, "action", step.ActionName)
	if action, err = job.Actions.Action(step.ActionName); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Failed to get action \"%v\" for step \"%v\": %v", step.ActionName, step.Name, err.Error()),
		)
	}

	if extraEnv != nil {
		if action.Env == nil {
			action.Env = make(variables.Env)
		}

		for key, value := range extraEnv {
			action.Env[key] = value
		}
	}

	// Create input context
	correlationId := job.ID.String()
	if _, ok := ctx["correlation_id"]; ok {
		correlationId = ctx["correlation_id"].(string)
	}
	runId := job.ExecutionID
	jobName := job.Name
	ctx["task_name"] = jobName
	ctx["run_id"] = runId
	ctx["correlation_id"] = correlationId
	ctx["step_name"] = step.Name
	ctx["action_name"] = action.Name
	containerTimeout := defaultTimeout
	if step.TimeoutSeconds > 0 {
		containerTimeout = time.Duration(step.TimeoutSeconds) * time.Second
	}

	level.Debug(actionLogger).Log("msg", "Starting Container", "timeout", containerTimeout)

	if inputJsonData, err = json.Marshal(ctx); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Invalid input context: %s", err.Error()),
		)
	}

	inputData := fmt.Sprintf("%s", inputJsonData)
	labels := make(map[string]string)
	labels["com.f5.pwe.job.runner"] = "kog"
	labels["com.f5.pwe.job.name"] = slugify.IDify(jobName, 60)
	labels["com.f5.pwe.job.step"] = slugify.IDify(step.Name, 60)
	labels["com.f5.pwe.job.action"] = slugify.IDify(action.Name, 60)
	labels["com.f5.pwe.job.run_id"] = slugify.IDify(runId.String(), 60)
	labels["com.f5.pwe.job.correlation_id"] = slugify.IDify(correlationId, 60)
	level.Debug(actionLogger).Log("labels", fmt.Sprintf("%+v", labels))

	// fire notification hook just before starting the container step
	if err := notifierHooks.FireNotification(job); err != nil {
		level.Error(actionLogger).Log("msg", "Notify hook failed", "err", err)
	}

	if err = executor.CreateJob(inputData, correlationId, jobName, step.Name, runId.String(), action, labels, volumes); err != nil {
		level.Error(actionLogger).Log("msg", "Error in create container", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	if err = executor.AttachIO(); err != nil {
		level.Error(actionLogger).Log("msg", "Error in attaching container IO", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	resultText := ""

	executor.ProcessOutput(&resultText)

	if err = executor.SendInput(inputData); err != nil {
		level.Error(actionLogger).Log("msg", "Error writing container stdin", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	level.Info(actionLogger).Log("msg", "Step Running")

	if exitCode, err = executor.Wait(containerTimeout); err != nil {
		level.Error(actionLogger).Log("msg", "Error waiting for step to complete", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	if result, err = DecodeResults([]byte(resultText), actionLogger); err != nil {
		level.Error(actionLogger).Log("msg", "Could not parse result", "err", err, "result", resultText)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	// Restore read-only keys
	result.Context["task_name"] = jobName
	result.Context["run_id"] = runId
	result.Context["correlation_id"] = correlationId
	result.Context["step_name"] = step.Name
	result.Context["action_name"] = action.Name
	result.ReturnCode = exitCode
	result.Name = step.Name

	return result

}

func NewStepResult(result actions.ActionStatus, message string) actions.ActionResult {
	return actions.ActionResult{
		Result:       result,
		ReturnCode:   -1,
		Message:      message,
		NotifyStatus: "",
		Context:      make(variables.Context),
	}
}

func NextStep(step workflow.WorkflowStep, code actions.ActionStatus) string {
	switch code {
	case actions.ACTION_SUCCESS:
		return step.SuccessStep
	case actions.ACTION_FAIL:
		return step.FailStep
	case actions.ACTION_ABORT:
		return step.AbortStep
	}
	return ""
}
