package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/variables"
	"gitlab.com/f5-pwe/kog"
)

var (
	executorType     string
	executor         kog.Executor
	jobFile          string
	outputFile       string
	contextFile      string
	correlationId    string
	workflow         string
	jobName          string
	extraEnv         map[string]string
	job              *pwe.Job
	startTime        time.Time
	volumes          []string
	envFiles         []string
	currentEnvImport bool
	resurnCodes      bool
	ignoreEnv        []string
)

func setupRunCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "run",
		Short: "Execute a task workflow",
	}

	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		level.Debug(baseLogger).Log("msg", "run PersistentPreRun called")
		startTime = time.Now()

		var (
			err    error
			reader io.Reader
			data   []byte
		)

		if reader, err = kog.FileOrURLReader(jobFile, baseLogger); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		if data, err = ioutil.ReadAll(reader); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		if job, err = kog.ParseJob(data); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		defaultExtraEnv()

		if contextFile != "" {
			if reader, err = kog.FileOrURLReader(contextFile, baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if data, err = ioutil.ReadAll(reader); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if ctxf, err := kog.ParseContext(data); err == nil {
				if subCtx, ok := ctxf["context"]; ok {
					ctxf = variables.Context(subCtx.(map[string]interface{}))
				}
				*job.Context = ctxf
			}
		}

		if workflow != "" {
			job.CurrentWorkflow = workflow
		}

		if job.Description == "" {
			job.Description = job.Name
		}

		if jobName != "" {
			job.Name = jobName
		}

		ctx := make(variables.Context)
		ctx["log_formatter"] = config.GetString("logger.formatter")
		ctx["log_level"] = config.GetString("logger.level")
		ctx["remote_logger"] = config.GetString("logger.remote")
		ctx["syslog_address"] = config.GetString("logger.addr")
		(*job.Context)["logging"] = ctx

		if correlationId != "" {
			(*job.Context)["correlation_id"] = correlationId
			
		} else if ((*job.Context)["correlation_id"] == nil || (*job.Context)["correlation_id"] == "") {
			(*job.Context)["correlation_id"] = job.ID.String()
		}

		baseLogger = log.With(
			baseLogger,
			"correlation_id", (*job.Context)["correlation_id"],
		)

		switch executorType {
		case "docker":
			var err error
			if executor, err = kog.NewDockerExecutor(baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}
		case "k8s":
			var err error
			if executor, err = kog.Newk8sExecutor(baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}
		default:
			level.Error(baseLogger).Log("err", fmt.Sprintf("unknown executor type: %s", executorType))
			os.Exit(127)
		}
	}

	cmd.Run = func(c *cobra.Command, args []string) {
		level.Info(baseLogger).Log("msg", "Starting Job", "description", job.Description)

		var result kog.JobResult

		defer func() {
			if outputFile != "" {
				var (
					ctxBytes []byte
					ctxData  string
				)
				ctxBytes, _ = json.Marshal(job.Context)

				if ctxBytes != nil {
					ctxData = fmt.Sprintf(`{"context":%s}`, string(ctxBytes))
				}

				if outputFile == "-" || outputFile == "stdout" {
					fmt.Println(ctxData)
				} else {
					level.Info(baseLogger).Log("msg", "Writing", "file", outputFile)
					if err := ioutil.WriteFile(outputFile, []byte(ctxData), 0644); err != nil {
						level.Error(baseLogger).Log("err", err)
					}
				}
			}
			if r := recover(); r != nil {
				if err := notifyStatus(job, kog.JobResult{Status: pwe.JOB_FAILURE}, baseLogger); err != nil {
					level.Error(baseLogger).Log("err", err)
				}
				level.Error(baseLogger).Log("err", r)
				os.Exit(127)
			}
			if err := notifyStatus(job, result, baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if resurnCodes && result.Status.ReturnCode() != 0 {
				os.Exit(result.Status.ReturnCode())
			}
		}()

		result = kog.ExecuteJob(executor, job, baseLogger, notifierHooks, extraEnv, volumes)

		resultLogger := log.With(baseLogger,
			"status", result.Status,
			"result_message", result.Message,
			"notify_status", result.NotifyStatus,
			"notify_message", result.NotifyMessage,
		)

		switch result.Status {
		case pwe.JOB_SUCCESS:
			level.Info(resultLogger).Log("msg", "Job Finished")
		case pwe.JOB_FAILURE:
			level.Error(resultLogger).Log("msg", "Job Failed")
		}
	}

	cmd.PersistentFlags().StringVarP(
		&executorType,
		"executor",
		"",
		"docker",
		"Executor to use [docker, k8s]")

	cmd.PersistentFlags().StringVarP(
		&jobFile,
		"file",
		"f",
		"",
		"Job File to execute")

	cmd.PersistentFlags().StringVarP(
		&workflow,
		"workflow",
		"w",
		"",
		"Workflow to execute within the Job file")

	cmd.PersistentFlags().StringToStringVarP(
		&extraEnv,
		"env",
		"e",
		nil,
		"KEY=VALUE pairs to pass to every workflow step as env vars")

	cmd.PersistentFlags().StringSliceVarP(
		&envFiles,
		"env-file",
		"",
		nil,
		"file with KEY=VALUE pairs on each line")

	cmd.PersistentFlags().BoolVarP(
		&currentEnvImport,
		"import-current-env",
		"",
		false,
		"load the current env vars")

	cmd.PersistentFlags().BoolVarP(
		&resurnCodes,
		"use-job-rc",
		"",
		true,
		"if kog should use return codes mapping to job status")

	cmd.PersistentFlags().StringSliceVarP(
		&ignoreEnv,
		"ignore-env",
		"",
		[]string{},
		"Ignore env vars  if they contain any of the args")

	cmd.PersistentFlags().StringSliceVarP(
		&volumes,
		"volume",
		"v",
		nil,
		"volumes to mount in step containers")

	cmd.PersistentFlags().StringVarP(
		&contextFile,
		"context",
		"c",
		"",
		"File to read context from")

	cmd.PersistentFlags().StringVarP(
		&correlationId,
		"correlation-id",
		"",
		"",
		"set your own correlation-id")

	cmd.PersistentFlags().StringVarP(
		&jobName,
		"job-name",
		"",
		os.Getenv("CI_JOB_NAME"),
		"set your own job name")

	cmd.PersistentFlags().StringVarP(
		&outputFile,
		"output",
		"o",
		"",
		"File to write the final context to")

	return
}

func notifyStatus(job *pwe.Job, result kog.JobResult, logger log.Logger) error {
	duration := time.Since(startTime)
	level.Debug(logger).Log("msg", "Job Duration", "duration", duration)

	if job == nil {
		return nil
	}

	if (result == kog.JobResult{}) {
		result = kog.JobResult{Status: pwe.JOB_FAILURE}
	}

	if result.NotifyStatus != "" {
		status := strings.ToLower(result.NotifyStatus)
		job.Status = pwe.JobStatusFromString(status)
		level.Debug(logger).Log("msg", "Updated job status", "status", result.NotifyStatus)
	} else {
		job.Status = result.Status
	}

	ctx := *job.Context
	if ctx != nil {
		ctx["duration_seconds"] = duration.Seconds()
	}

	if err := notifierHooks.FireNotification(job); err != nil {

		return errors.Wrap(err, "notify hook failed")
	}

	return nil
}

func defaultExtraEnv() {
	var (
		ok     bool
		err    error
		reader io.Reader
		myEnv  map[string]string
	)
	if extraEnv == nil {
		extraEnv = make(map[string]string)
	}

	for _, envFile := range envFiles {
		if reader, err = kog.FileOrURLReader(envFile, baseLogger); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		if myEnv, err = godotenv.Parse(reader); err != nil {
			level.Error(baseLogger).Log("err", err)
			os.Exit(127)
		}

		for k, v := range myEnv {
			if _, ok := extraEnv[k]; ok {
				continue
			}
			extraEnv[k] = v
		}
	}

	if currentEnvImport {
		var items []string
		var key, value string
	OUTER:
		for _, pair := range os.Environ() {
			items = strings.Split(pair, "=")
			key = items[0]
			if _, ok = extraEnv[key]; ok {
				//cmd --env overrides current env vars
				continue
			}

			value = items[1]
			for _, ignore := range ignoreEnv {
				if strings.Contains(key, ignore) {
					continue OUTER
				}
			}
			extraEnv[key] = value
		}

	}
}
