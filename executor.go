package kog

import (
	"io"
	"time"

	pwe "gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/actions"
)

const containerExitTimeout = time.Second * 30
const containerStartTimeout = time.Minute * 15

type Job interface {
	ID() string
	Input(input io.WriteCloser) io.WriteCloser
	Output(output io.ReadCloser) io.ReadCloser
}

type Executor interface {
	Prepare(job *pwe.Job) error
	CreateJob(inputData, correlationId, jobName, stepName, runId string, action actions.Action, labels map[string]string, volumes []string) error
	GetJob() Job
	AttachIO() error
	SendInput(inputData string) error
	ProcessOutput(resultText *string)
	Wait(timeout time.Duration) (int, error)
	Cleanup() error
}
