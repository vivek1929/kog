package main

import (
	"io"
	"io/ioutil"
	"os"

	"github.com/go-kit/kit/log/level"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/f5-pwe/common/variables"
	"gitlab.com/f5-pwe/kog"
)

func setupValidateCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "validate",
		Short: "validate workflow file",
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		level.Debug(baseLogger).Log("msg", "validate PersistentPreRun called")
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		var (
			err    error
			reader io.Reader
			data   []byte
		)

		if jobFile != "" {
			if reader, err = kog.FileOrURLReader(jobFile, baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}
			level.Info(baseLogger).Log("msg", "Validating Workflow File", "file", jobFile)

			if data, err = ioutil.ReadAll(reader); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if job, err = kog.ParseJob(data); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}
		}

		if contextFile != "" {
			level.Info(baseLogger).Log("msg", "Validating Context File", "file", contextFile)
			if reader, err = kog.FileOrURLReader(contextFile, baseLogger); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if data, err = ioutil.ReadAll(reader); err != nil {
				level.Error(baseLogger).Log("err", err)
				os.Exit(127)
			}

			if ctxf, err := kog.ParseContext(data); err == nil {
				if subCtx, ok := ctxf["context"]; ok {
					ctxf = variables.Context(subCtx.(map[string]interface{}))
				} else {
					level.Error(baseLogger).Log("err", err)
					os.Exit(127)
				}
				ctxf["validated"] = true
			}
		}
	}

	cmd.PersistentFlags().StringVarP(
		&jobFile,
		"file",
		"f",
		"",
		"Job File to execute")

	cmd.PersistentFlags().StringVarP(
		&contextFile,
		"context",
		"c",
		"",
		"File to read context from")

	return
}
