

<!--- next entry here -->

## 1.5.9
2019-11-22

### Fixes

- To avoid timeout issue when action result received before completion (94cc5dfe2ac7245183fb1731beb3e0983a3e78a3)

## 1.5.8
2019-08-23

### Fixes

- add correlation id to logs send to logstash (57abbf7c5626df27dbfe4739e7fca24eaea187cd)
- log stdout from container (52ef595c0780e11a1e8b358731a219db57044839)

## 1.5.7
2019-08-02

### Fixes

- Shorten the job name to 50 chars, Update the event listener to not overwrite the error msg (53878dacfd354127386447eda2e3976646c655ce)
- executor.k8s.go create job name from correlationId + runId + stepName + jobName (02f531cabeaef992ac9a1e44d024dde7682398f5)

## 1.5.6
2019-07-29

### Fixes

- Update executor.k8s.job.go to pass delete options (7a70755a42bdf825f56752c880ad305c6e959632)

## 1.5.5
2019-07-20

### Fixes

- vendor the deps to speed up the build and test stages. Also add the lint to the pipeline (b2b31fe018e8c1cc5580791c0981e4fa343fde0e)

## 1.5.4
2019-07-20

### Fixes

- notifiers we not propperly being loaded (cb2f33d1d36f98f6a3749dd5edb17385f78d6aba)

## 1.5.3
2019-06-10

### Fixes

- make containerStartTimeout 15 min (b35e8905a3c8dacbf72688011c1e22447ca8003b)

## 1.5.2
2019-06-07

### Fixes

- fix type [ci-skip] (b0a3fc4f17931389fcf00edb1d8d94ce8dacdc75)
- always pull image (88cf8f47314dd398abfa697a28c4483b83091e26)

## 1.5.1
2019-05-10

### Fixes

- update the readme files to be more detailed (2498776c35b05b647b9e25b7bbe0520242b1ae9b)

## 1.5.0
2019-05-10

### Features

- move the env vars to a k8s secret and load them in with envFrom (2acc5f175e82b6af53140dc0178ec6c3e0e6f2c1)

## 1.4.5
2019-05-09

### Fixes

- handle the case where k8s will drop the log stream due to to many open file handles (6b649e68740a52173e26527d138a28a5a6fa5d9d)

## 1.4.4
2019-05-01

### Fixes

- DNS-1123 subdomain validation on k8s when job names, or step names have _ in them (737effdbe8e35d5fc0c502fdf680aff961464b29)

## 1.4.3
2019-05-01

### Fixes

- ensure you can run jobs with names longer then 63 characters (f70e575ca90e95b54b5d6a8b740ec8c890283f0c)

## 1.4.2
2019-05-01

### Fixes

- filter out versions you cannot switch-to to (d6e5fa3db3b684482fe65d43124ce6eb0fcb6ee2)

## 1.4.1
2019-05-01

### Fixes

- replace fmt.Printf with level.Debug logging (71eea505318450852b0588b38ed23303c7023492)

## 1.4.0
2019-05-01

### Features

- add switch-to / upgrade command to allow easier local upgrades, and easier support of multiple versions (639ff680e6bc4c11d40f8bc35922c9474d66a562)

## 1.3.1
2019-04-30

### Fixes

- reduce the number of labels (19a3064e27b3b1d7657e6313c0f931e68a74feb5)

## 1.3.0
2019-04-30

### Features

- add version and validate commands (467cd998a68f068e3db181cee135ef7399377019)

## 1.2.4
2019-04-30

### Fixes

- stop vendoring the dependencies (c2976972332f540841aa45a508d20f0a79406d3f)

## 1.2.3
2019-04-30

### Fixes

- ensure the stdout processor of the docker executor exits if it recived an EOF (d4c99429c9dfa6cc2a3f0f22751bc1ed32fa8def)

## 1.2.2
2019-04-26

### Fixes

- flattent out the kog structure to not have sub resources (d68b1321f531e5cba48f3f7261a6a0c1bcc02455)

## 1.2.1
2019-04-26

### Fixes

- ensure catching issue if the pod failed to run (cfb7a5babdf7e35ec2fe849885b65e9c34b18eee)

## 1.2.0
2019-04-25

### Features

- first pass at actual k8s job execution working. Much cleanup needed (fcb0564c97f9cd17ade707bea4cddf772dc30e15)

### Fixes

- cleanup IO for k8s & docker executor (f1572fb8a2c4a60d0c7db795f3ff14cb9dc3ae62)
- update deps (73eb682278b3db97cb6a426d1820b6a93fad77e6)

## 1.1.2
2019-04-25

### Fixes

- cleanup IO for k8s & docker executor (91b11b2bcbe4346b60af42ba300a02368be6cd0c)

## 1.1.1
2019-04-20

### Fixes

- resolve a simple logic error with how sync.WaitGroup was being used (f5a0fc8ead313f80597d4dfb55ef70a2e76836ea)

## 1.1.0
2019-04-19

### Features

- split out executor and add initial support for k8s executor (0c203c9f3b9a5568178f038ef899eb98d671d64c)
- clean up the docker log demuxer (11c5a121c715be46ba82e143d0b9877122c97889)

### Fixes

- stop throwing an error when the docker container fails to delete as that is not a job failure (2ecf8bb013df6a5061e23021015834191dbb3a38)

## 1.0.13
2019-04-19

### Fixes

- stop throwing an error when the docker container fails to delete as that is not a job failure (0fdb4ab20404db8293ad0a187a3f38ce972ada62)

## 1.0.12
2019-04-18

### Fixes

- resolve panic due to null error access (cc22883b03aed66a254ebe3ccf5f4efafd809b2d)

## 1.0.11
2019-04-18

### Fixes

- rearange the container calls to make it more likely that the container not found error is caught properly (6dafb5e4e25c4032f2b9a2630c521ee75fe3a1cb)

## 1.0.10
2019-04-18

### Fixes

- override stdcopy.go to return more info about the error (01b4d811c69f4b6643b9f6290e8501c9d90c53fd)

## 1.0.9
2019-04-17

### Fixes

- stop masking the image not found error and default to debug logging for now (844464271700cb2f4e6746ae56bbaf02c4243681)

## 1.0.8
2019-04-04

### Fixes

- make sure SEMVER is set when adding the download links (8202ed49fb37e761a9dd96c7b4eee78f575af363)

## 1.0.7
2019-04-04

### Fixes

- use bintray to store the artifacts since the gitlab upload file requires auth (e4048098b77aad47e7b43ac055e7a05b04d07964)

## 1.0.6
2019-04-03

### Fixes

- adjust the init.js to not error when trying to write the binary (85576a8053a1ab7fa2051815e979bd41242dcc90)

## 1.0.5
2019-04-03

### Fixes

- ensure npm is published as public (dc47adfb0b02c7badea61eb65ebd5adcb653b938)

## 1.0.4
2019-04-03

### Fixes

- adjust scope of the package since there was a 2 year old package called kog that has no files in it (0393c152280ca8ef9e3aa7db61d2abaa7922b5ac)

## 1.0.3
2019-04-03

### Fixes

- Add required bundledDependencies (7fb4115e0cf7adc28cae4fdb6aa1a3f1dcfb3699)
- clean up lint for npm package (e25bddd405445b05bc7ac57c3dbedc368c2c139b)

## 1.0.2
2019-04-01

### Fixes

- make kog more resilient to images being deleted between stages of a workflow (2714f5f3282b16f1a4216efb5497ee40e5779dca)

## 1.0.1
2019-03-29

### Fixes

- make sure to refetch the imageInfo after pulling the image again (30f0fc0fd623f1896d7933a069eeaec6caf9e95a)

## 1.0.0
2019-03-29

### Fixes

- switch to go modules (1ea09f92bff0253c554990c41b6c5461b27981d0)

