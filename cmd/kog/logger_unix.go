// +build aix darwin,!arm,!arm64 dragonfly freebsd js,wasm linux nacl netbsd openbsd solaris

package main

import (
	gosyslog "log/syslog"
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/log/syslog"
	"github.com/spf13/viper"
)

func setupLogger(cfg *viper.Viper) (logger log.Logger) {
	cfg.SetDefault("logger.formatter", "logfmt")
	cfg.SetDefault("logger.level", "info")
	logFmtr := log.NewLogfmtLogger
	switch cfg.GetString("logger.formatter") {
	case "json":
		logger = log.NewJSONLogger(os.Stderr)
		logFmtr = log.NewJSONLogger
	case "logfmt":
		logger = log.NewLogfmtLogger(os.Stderr)
	default:
		logger = log.NewLogfmtLogger(os.Stderr)
		logger.Log("err", "Unknown Log Formatter", "logger.formatter", cfg.GetString("logger.formatter"))
		os.Exit(127)
	}

	lvl := cfg.GetString("logger.level")
	if cfg.GetBool("debug") {
		lvl = "debug"
	}

	switch cfg.GetString("logger.remote") {
	case "syslog":
		syslogLvl := gosyslog.LOG_EMERG
		switch lvl {
		case "error":
			syslogLvl = gosyslog.LOG_ERR
		case "warn":
			syslogLvl = gosyslog.LOG_WARNING
		case "info":
			syslogLvl = gosyslog.LOG_INFO
		case "debug":
			syslogLvl = gosyslog.LOG_DEBUG
		}
		w, err := gosyslog.Dial(
			cfg.GetString("logger.proto"),
			cfg.GetString("logger.addr"),
			syslogLvl|gosyslog.LOG_USER,
			pkgName)
		if err != nil {
			logger.Log("err", err)
			os.Exit(127)
		}
		logger = syslog.NewSyslogLogger(w, logFmtr)
	}

	switch lvl {
	case "none":
		logger = level.NewFilter(logger, level.AllowNone())
	case "error":
		logger = level.NewFilter(logger, level.AllowError())
	case "warn":
		logger = level.NewFilter(logger, level.AllowWarn())
	case "debug":
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger, "commit", commit)
	default:
		logger = level.NewFilter(logger, level.AllowInfo())
	}

	logger = level.NewInjector(logger, level.DebugValue())

	logger = log.With(
		logger,
		"timestamp", log.DefaultTimestampUTC,
		"version", version,
	)

	return
}
