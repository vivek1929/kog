validateNpmVersion();

const https = require('follow-redirects').https;
const fs = require('fs');
const packageJson = require('./package.json');
const fileName = getFileName();
const filePath = 'bin/' + fileName;
const version = packageJson.version;
const arch = getArchitecture();
const releaseURL = 'https://gitlab.com/api/v4/projects/5998824/releases/v' + version + '/assets/links';

fetchReleases();

function validateNpmVersion() {
  if (!isValidNpmVersion()) {
    throw new Error('kog CLI can be installed using npm version 5.0.0 or above.');
  }
}

function fetchReleases() {
  console.log('Fetching kog CLI release list ' + releaseURL );
  https.get(releaseURL, parseReleases).on('error', function (err) {console.error(err);});
}


function downloadCli(url) {
  console.log('Downloading kog CLI ' + version + ' from ' + url);
  https.get(url, writeToFile).on('error', function (err) {console.error(err);});
}

function isValidNpmVersion() {
  var child_process = require('child_process');
  var npmVersionCmdOut = child_process.execSync('npm version -json');
  var npmVersion = JSON.parse(npmVersionCmdOut).npm;
  // Supported since version 5.0.0
  return parseInt(npmVersion.charAt(0)) > 4;
}

function parseReleases(response) {
  var artifactsJSON = '';

  response.on('data', function (chunk) {
    artifactsJSON += chunk;
  }).on('end', function () {
    var releases = JSON.parse(artifactsJSON);
    releases.forEach(function(release){
      if(release.name == arch) {
        downloadCli(release.url);
        return;
      }
    });
  }).on('error', function (err) {
    console.error(err);
  });
}

function writeToFile(response) {
  var exe = fs.createWriteStream(filePath);
  response.on('data', function (chunk) {
    exe.write(chunk);
  }).on('end', function () {
    exe.end();
  }).on('error', function (err) {
    console.error(err);
  });

  exe.on('finish', function(){
    if (!process.platform.startsWith('win')) {
      fs.chmodSync(filePath, 0o0755);
    }
  });
}

function getArchitecture() {
  var platform = process.platform;
  if (platform.startsWith('win')) {
    return 'windows-amd64';
  }
  if (platform.includes('darwin')) {
    return 'darwin-amd64';
  }
  if (process.arch.includes('64')) {
    return 'linux-amd64';
  }
  return 'linux-386';
}

function getFileName() {
  var excecutable = 'kog';
  if (process.platform.startsWith('win')) {
    excecutable += '.exe';
  }
  return excecutable;
}
