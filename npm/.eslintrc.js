module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "plugins": [
        "ava",
    ],
    "extends": [
        "eslint:recommended",
        "plugin:ava/recommended",
    ],
    "parserOptions": {
        "ecmaVersion": 8,
        "sourceType": "module",
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
        }
    },
    "rules": {
        "no-console": 0,
        "indent": [
            "error",
            2
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
