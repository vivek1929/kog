package kog

import (
	"io"

	batchapiv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	batchv1 "k8s.io/client-go/kubernetes/typed/batch/v1"
)

type k8sJob struct {
	k8sJobName string
	k8sType    *batchapiv1.Job
	pod        *apiv1.Pod
	client     batchv1.JobInterface
	output     io.ReadCloser
	input      io.WriteCloser
	logsSince  metav1.Time
}

func (kj *k8sJob) ID() string {
	return kj.k8sType.Name
}

func (kj *k8sJob) Create() (err error) {
	kj.k8sType, err = kj.client.Create(kj.k8sType)

	return
}

func (kj *k8sJob) Delete() (err error) {
        deletePolicy := metav1.DeletePropagationForeground
	return kj.client.Delete(kj.ID(), &metav1.DeleteOptions{PropagationPolicy: &deletePolicy})
}

func (kj *k8sJob) UpdateStatus() (err error) {
	kj.k8sType, err = kj.client.UpdateStatus(kj.k8sType)

	return
}

func (kj *k8sJob) Update() (err error) {
	kj.k8sType, err = kj.client.Update(kj.k8sType)

	return
}

func (kj *k8sJob) Watch() (watcher watch.Interface, err error) {
	watcher, err = kj.client.Watch(metav1.SingleObject(kj.k8sType.ObjectMeta))

	return
}

func (kj *k8sJob) Input(input io.WriteCloser) io.WriteCloser {
	if input != nil {
		kj.input = input
	}

	return kj.input
}

func (kj *k8sJob) Output(output io.ReadCloser) io.ReadCloser {
	if output != nil {
		kj.output = output
	}

	return kj.output
}
