package kog

import (
	"encoding/json"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	pwe "gitlab.com/f5-pwe/common"
)

// consoleHook prints the body to console, useful for debugging
type consoleHook struct {
	logger log.Logger
}

func NewConsoleHook(logger log.Logger) NotifyHook {
	return &consoleHook{
		logger: log.With(logger, "component", "console-notifier"),
	}
}

func (c *consoleHook) Logger() log.Logger {
	return c.logger
}

func (c *consoleHook) FireNotification(job *pwe.Job) error {
	body, err := json.Marshal(job)
	if err != nil {
		return err
	}
	level.Debug(c.logger).Log("msg", "Console hook fired", "body", string(body))
	return nil
}
